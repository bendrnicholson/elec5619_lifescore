-- MySQL Script generated by MySQL Workbench
-- Thu Sep 29 00:29:25 2016
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema lifescore
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema lifescore
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `lifescore` DEFAULT CHARACTER SET utf8 ;
USE `lifescore` ;

-- -----------------------------------------------------
-- Table `lifescore`.`Activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lifescore`.`Activity` (
  `name` VARCHAR(45) NOT NULL,
  `pointsPerMinute` INT(11) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lifescore`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lifescore`.`Users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(20) NOT NULL,
  `email` VARCHAR(50) NULL DEFAULT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(45) NOT NULL,
  `startDate` DATETIME NOT NULL,
  `occupation` VARCHAR(45) NULL DEFAULT NULL,
  `dob` DATETIME NULL DEFAULT NULL,
  `gender` VARCHAR(45) NULL DEFAULT NULL,
  `location` VARCHAR(45) NULL DEFAULT NULL,
  `totalScore` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `id_UNIQUE` (`userName` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lifescore`.`DataLog`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lifescore`.`DataLog` (
  `timestamp` DATETIME NOT NULL,
  `minutesSinceLastLog` INT(11) NULL DEFAULT NULL,
  `positiveTime` INT(11) NULL DEFAULT NULL,
  `negativeTime` INT(11) NULL DEFAULT NULL,
  `neutralTime` INT(11) NULL DEFAULT NULL,
  `score` INT(11) NULL DEFAULT NULL,
  `user_id` VARCHAR(20) NOT NULL,
  `dateString` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`timestamp`, `user_id`))
  
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lifescore`.`DayLog`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lifescore`.`DayLog` (
  `date` DATETIME NOT NULL,
  `dailyScore` INT(11) NULL DEFAULT NULL,
  `user_id` VARCHAR(20) NOT NULL,
  `dateString` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`date`, `user_id`),
  INDEX `fk_Day_Users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_Day_Users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `lifescore`.`Users` (`userName`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `lifescore`.`Friendship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lifescore`.`Friendship` (
  `user_id` VARCHAR(20) NOT NULL,
  `friend_id` VARCHAR(20) NOT NULL,
  `timestamp` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`, `friend_id`),
  INDEX `fk_Users_has_Users_Users2_idx` (`friend_id` ASC),
  INDEX `fk_Users_has_Users_Users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_Users_has_Users_Users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `lifescore`.`Users` (`userName`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Users_has_Users_Users2`
    FOREIGN KEY (`friend_id`)
    REFERENCES `lifescore`.`Users` (`userName`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
