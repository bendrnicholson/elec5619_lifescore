# README #

This readme will tell you how to set up this project on your own machine, and how to run the app, Lifescore.

* * * 

#Setup#

##Environment##

* Download the IDE, Spring Tool Suite (STS) from https://spring.io/tools/sts
* Ensure that you have Apache Tomcat version 8 downloaded. This is available at http://tomcat.apache.org/ on the left hand column of the page.
* Install Tomcat, using this tutorial: https://www.youtube.com/watch?v=n14rpj_08wM. This tutorial will teach you how to:
    * Download Tomcat
    * Add the downloaded server into STS
    * Set up the server for the Spring project
    * Run the server on localhost

##Codebase##
* Download or clone the source code from this repository, using SourceTree or the command line.
* Once you have the files locally on your machine, you can open it up with the IDE (STS) that was downloaded earlier.

##Database##
* Download MySQL workbench from http://www.mysql.com/products/workbench/
* Download MySQL community server and set it up https://dev.mysql.com/downloads/mysql/
* Once the MySQL server is set up on your computer, and you have MySQL workbench, run the SQL script included in this project called 'lifescore.sql'. This script will set up the schema and database required for Lifescore
* There are other scripts included to bootstrap (add initial data) to the database, however they are not required. (example: activity_input.sql)

#Deploy#
* In STS, start the Tomcat server by clicking the green arrow button. This will start the application and deploy it to the localhost server.
* Once the build and deployment to localhost has completed, navigate to localhost:8080/elec5619/ and enjoy the app.
