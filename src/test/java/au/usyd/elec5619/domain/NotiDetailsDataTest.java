package au.usyd.elec5619.domain;

import java.time.LocalDateTime;

import junit.framework.TestCase;

public class NotiDetailsDataTest extends TestCase {

	private NotiDetailsData notiDetailData;
	
	protected void setUp() throws Exception {
		notiDetailData = new NotiDetailsData();
	}
	
	public void testSetAndGetFirstName() {
		String testFirstName = "FirstName";
		assertNull(notiDetailData.getFirstName());
		notiDetailData.setFirstName(testFirstName);
		assertEquals(notiDetailData.getFirstName(), testFirstName);
	}
	
	public void testSetAndGetId() {
		long testId = 123456;
		assertEquals(notiDetailData.getId(), 0);
		notiDetailData.setId(testId);
		assertEquals(notiDetailData.getId(), testId);
	}
	
	public void testSetAndGetEmail() {
		String testEmail = "12345@gmail.com";
		assertNull(notiDetailData.getEmail());
		notiDetailData.setEmail(testEmail);
		assertEquals(notiDetailData.getEmail(), testEmail);
	}
	
	public void testSetAndGetNeedNotify() {
		boolean testNeedNotify = true;
		assertFalse(notiDetailData.isNeedNotify());
		notiDetailData.setNeedNotify(testNeedNotify);;
		assertEquals(notiDetailData.isNeedNotify(), testNeedNotify);
	}
	
	public void testSetAndGetIntervalTime() {
		int testIntervalTime = 6;
		assertEquals(notiDetailData.getIntervalTime(), 0);
		notiDetailData.setIntervalTime(testIntervalTime);
		assertEquals(notiDetailData.getIntervalTime(), testIntervalTime);
	}
	
	public void testSetAndGetNotiTime() {
		LocalDateTime testNotiTime = LocalDateTime.now();
		assertNull(notiDetailData.getNotiTime());
		notiDetailData.setNotiTime(testNotiTime);;
		assertEquals(notiDetailData.getNotiTime(), testNotiTime);
	}
	
}
