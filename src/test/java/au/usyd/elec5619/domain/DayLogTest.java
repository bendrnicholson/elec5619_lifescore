package au.usyd.elec5619.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class DayLogTest extends TestCase {

    private DayLog daylog;

    protected void setUp() throws Exception {
        daylog = new DayLog();
    }
    
    public void testSetAndGetDailyScore(){
    	long testDailyScore = 10;
    	assertEquals(daylog.getDailyScore(), 0);    	
    	daylog.setDailyScore(testDailyScore);
    	assertEquals(testDailyScore,daylog.getDailyScore());
    }
    
    public void testSetAndGetDate(){
    	Date testDate = new Date();
    	assertEquals(daylog.getDate(), null);    	
    	daylog.setDate(testDate);
    	assertEquals(testDate,daylog.getDate());
    }
    
    public void testSetAndGetId(){
    	long testid = 1;
    	assertEquals(daylog.getId(), 0);    	
    	daylog.setId(testid);
    	assertEquals(testid,daylog.getId());
    }
    
    public void testSetAndGetUser(){
    	User testuser = new User();
    	assertEquals(daylog.getUser(), null);    	
    	daylog.setUser(testuser);
    	assertEquals(testuser,daylog.getUser());
    }
    


}
