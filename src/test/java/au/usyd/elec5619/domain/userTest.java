package au.usyd.elec5619.domain;

import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class userTest extends TestCase  {
	private User user;
	public void setUp() throws Exception{
		user = new User();
	}
	
	public void testSetandGetID(){
		long id = 1;
		assertEquals(0,0);
		user.setId(id);
		assertEquals(id,user.getId());
	}
	
	public void testSetandGetname(){
		String name = "test";
		assertEquals(0,0);
		user.setFirstName(name);
		assertEquals(name,user.getFirstName());
	}
	
	public void testFriend(){
		Set<User> tlist = new HashSet<User>();
		User fuser = new User();
		tlist.add(fuser);
		assertEquals(0,0);
		user.setFriends(tlist);
		assertEquals(tlist,user.getfriends());
	}
	
	public void testSetandGeteMAIL(){
		String te = "123@gmial.com";
		assertEquals(0,0);
		user.setEmail(te);
		assertEquals(te,user.getEmail());
	}
}
