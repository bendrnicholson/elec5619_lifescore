package au.usyd.elec5619.domain;

import java.time.LocalDateTime;

import junit.framework.TestCase;

public class NotiLogDataTest extends TestCase {

private NotiLogData notiLogData;
	
	protected void setUp() throws Exception {
		notiLogData = new NotiLogData();
	}
	
	public void testSetAndGetId() {
		long testId = 123456;
		assertEquals(notiLogData.getId(), 0);
		notiLogData.setId(testId);
		assertEquals(notiLogData.getId(), testId);
	}
	
	public void testSetAndGetNotiContent() {
		String testNotiContent = "qwertyuiopasdfghjklzxcvbnm";
		assertNull(notiLogData.getNotiContent());
		notiLogData.setNotiContent(testNotiContent);
		assertEquals(notiLogData.getNotiContent(), testNotiContent);
	}
	
	public void testSetAndGetDate() {
		LocalDateTime testDateTime = LocalDateTime.now();
		assertNull(notiLogData.getDateTime());
		notiLogData.setDateTime(testDateTime);
		assertEquals(notiLogData.getDateTime(), testDateTime);
	}
	
	public void testSetAndGetLogId() {
		long testLogId = 123456;
		assertEquals(notiLogData.getId(), 0);
		notiLogData.setId(testLogId);
		assertEquals(notiLogData.getId(), testLogId);
	}
	
	
}
