package au.usyd.elec5619.service;

import static org.junit.Assert.*;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import au.usyd.elec5619.domain.Activity;
import au.usyd.elec5619.service.IActivityManager;

public class ActivityTest {

	@Resource(name="activityManager")
	private IActivityManager activityManager;
	
	@Test
	public void testListActivityEmpty(){
		List<Activity> list = activityManager.listActivity();
		assertEquals(0,  list.size());
	}
	
	@Test
	public void testListActivity1(){
		List<Activity> list = activityManager.listActivity();
		Activity a1 = new Activity();
		a1.setActivityId(1);
		a1.setActivityName("Swmming");
		a1.setPointsPerMinute(5);
		list.add(a1);
		assertNotNull(list);
	}
	
	@Test
	public void testListActivity2(){
		List<Activity> list = activityManager.listActivity();
		Activity a1 = new Activity();
		a1.setActivityId(1);
		a1.setActivityName("Swmming");
		a1.setPointsPerMinute(5);
		list.add(a1);
		assertEquals(1,  list.size());
	}
	
	@Test
	public void testGetActivityById1(){
		Activity a = activityManager.getActivityById(1);
		assertEquals("swimming", a.getActivityName());
	}
	
	@Test
	public void testGetActivityById2(){
		Activity a = activityManager.getActivityById(1);
		assertEquals(1, a.getActivityId());
		
	}
}
