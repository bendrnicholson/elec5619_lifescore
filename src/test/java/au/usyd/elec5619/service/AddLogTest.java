package au.usyd.elec5619.service;

import static org.junit.Assert.*;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.usyd.elec5619.domain.Activity;
import au.usyd.elec5619.domain.DataLog;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.IDataManager;
import au.usyd.elec5619.service.IUserManager;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class AddLogTest {
	
	@Resource(name="dataManager")
	private IDataManager dataManager;
	
	@Test
    public void testAddDataLog(){	
		DataLog dataLog = new DataLog();
		dataLog.setActivity(new Activity());
		dataLog.setId(1);
		dataLog.setPeriodtime(60);
		dataLog.setScore(50);
		dataLog.setTimestamp(new Date());
		dataLog.setUser(new User());
		dataManager.addData(dataLog);
	}
	
	@Test
	public void testGetDataLogById1(){
		DataLog dataLog = dataManager.getDataById(1);
		assertNotNull(dataLog);
	}
	
	@Test
	public void testGetDataLogById2(){
		DataLog dataLog = dataManager.getDataById(1);
		assertEquals(1, dataLog.getUser().getId());
	}
	
	
	
}
