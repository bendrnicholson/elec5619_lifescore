package au.usyd.elec5619.service;

import java.util.List;

import au.usyd.elec5619.domain.User;
import junit.framework.TestCase;

public class UserManagerTestL extends TestCase{
	private UserManager userManager;
	private List<User> users;
	protected void setUp() throws Exception{
		userManager = new UserManager();
		User u = new User();
		u.setId(100);
		u.setUserName("test");
		u.setPassword("123");
		u.setEmail("123@gmail.com");
		userManager.addUser(u);
		
		
		u= new User();
		u.setId(101);
		u.setUserName("test2");
		u.setPassword("321");
		u.setEmail("321@gmail.com");
		userManager.addUser(u);
		
		
	}
	public void testNoUser(){
		userManager = new UserManager();
		assertEquals(0,0);
		assertEquals(null,userManager.listUsers());
	}
	
}
