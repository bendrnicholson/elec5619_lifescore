package au.usyd.elec5619.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.usyd.elec5619.domain.NotiDetailsData;
import au.usyd.elec5619.domain.NotiLogData;
import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class NotificationServiceTest extends TestCase {
	
	@Resource(name = "notificationService")
	private NotificationServiceInterface notificationServiceInterface;
	private List<NotiLogData> notiLogDatas;
	
	private static int NOTILGODATA_COUNT = 3;
	
	//NotiDetailsData sample data
	private static long A_ID = 123456;
	private static String A_EMAIL = "qwer@tyu.com";
	private static boolean A_NEEDNOTI = true;
	
	private static long B_ID = 123456;
	private static String B_EMAIL = "m908kj@tyu.com";
	private static boolean B_NEEDNOTI = false;
	
	private static long C_ID = 123456;
	private static String C_EMAIL = "mmmm@987.com";
	private static boolean C_NEEDNOTI = true;
	
	//NotiLogData sample data
	private static String N1_NOTICONTENT = "aaaaaaaaaaaaaaaaaaa";
	private static LocalDateTime N1_DATE = LocalDateTime.of(1997, 10, 2, 0, 0);
	
	private static String N2_NOTICONTENT = "bbbbbbbbbbbbbbbbb";
	private static LocalDateTime N2_DATE = LocalDateTime.of(2009, 4, 2, 0, 0);
	
	private static String N3_NOTICONTENT = "cccccccccccccccccc";
	private static LocalDateTime N3_DATE = LocalDateTime.of(2012, 1, 4, 0, 0);
	
	private static String N4_NOTICONTENT = "ddddddddddddddddddd";
	private static LocalDateTime N4_DATE = LocalDateTime.of(1988, 6, 14, 0, 0);
	
	private static String N5_NOTICONTENT = "eeeeeeeeeeeeeeeeee";
	private static LocalDateTime N5_DATE = LocalDateTime.of(2046, 11, 2, 0, 0);

	@Test
	public void setUp() throws Exception {
		
		NotiDetailsData notiDetailsData = new NotiDetailsData();
		notiDetailsData.setId(A_ID);
		notiDetailsData.setEmail(A_EMAIL);
		notiDetailsData.setNeedNotify(A_NEEDNOTI);
		notificationServiceInterface.updateNotiDetailsData(notiDetailsData);
		
		notiDetailsData = new NotiDetailsData();
		notiDetailsData.setId(B_ID);
		notiDetailsData.setEmail(B_EMAIL);
		notiDetailsData.setNeedNotify(B_NEEDNOTI);
		notificationServiceInterface.updateNotiDetailsData(notiDetailsData);
		
		notiDetailsData = new NotiDetailsData();
		notiDetailsData.setId(C_ID);
		notiDetailsData.setEmail(C_EMAIL);
		notiDetailsData.setNeedNotify(C_NEEDNOTI);
		notificationServiceInterface.updateNotiDetailsData(notiDetailsData);
		
		NotiLogData notiLogData = new NotiLogData();
		notiLogData.setId(A_ID);
		notiLogData.setNotiContent(N1_NOTICONTENT);
		notiLogData.setDateTime(N1_DATE);
		notificationServiceInterface.addNotiLogData(notiLogData);
		
		notiLogData = new NotiLogData();
		notiLogData.setId(A_ID);
		notiLogData.setNotiContent(N2_NOTICONTENT);
		notiLogData.setDateTime(N2_DATE);
		notificationServiceInterface.addNotiLogData(notiLogData);
		
		notiLogData = new NotiLogData();
		notiLogData.setId(A_ID);
		notiLogData.setNotiContent(N3_NOTICONTENT);
		notiLogData.setDateTime(N3_DATE);
		notificationServiceInterface.addNotiLogData(notiLogData);
		
		notiLogData = new NotiLogData();
		notiLogData.setId(B_ID);
		notiLogData.setNotiContent(N4_NOTICONTENT);
		notiLogData.setDateTime(N4_DATE);
		notificationServiceInterface.addNotiLogData(notiLogData);
		
		notiLogData = new NotiLogData();
		notiLogData.setId(B_ID);
		notiLogData.setNotiContent(N5_NOTICONTENT);
		notiLogData.setDateTime(N5_DATE);
		notificationServiceInterface.addNotiLogData(notiLogData);
	}
	
	@Test
	public void testGetNotiDetailsDataWithNoValue() {
		assertNull(notificationServiceInterface.getNotiDetailsDataById(A_ID));
	}
	
	@Test
	public void testGetNotiDetailsData() {
		NotiDetailsData notiDetailsData = notificationServiceInterface.getNotiDetailsDataById(A_ID);
		assertNotNull(notiDetailsData);
	}
	
}
