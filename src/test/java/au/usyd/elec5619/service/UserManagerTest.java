package au.usyd.elec5619.service;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.IUserManager;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class UserManagerTest {
	@Resource(name="userManager")
	private IUserManager userManager;
	
	@Test
    public void testAddUser(){	
		User user = new User();
		user.setDob(new Date());
		user.setEmail("123@gmail.com");
		user.setFirstName("Tom");
		user.setGender("male");
		user.setLastName("love");
		user.setLocation("syd");
		user.setOccupation("student");
		user.setPassword("123");
		user.setUserName("Eva");
		user.setStartDate(new Date());
		userManager.addUser(user);
		
	}
	
	@Test
    public void testListUser(){
		// including the one we just added above
		Assert.assertEquals(6, userManager.listUsers().size());
	}
	
	@Test
    public void testGetUserById(){
		Assert.assertEquals(1, userManager.getUserById(1).getId());
	}
	
	@Test
    public void testUpdateUser(){
		User user = userManager.getUserById(1);
		user.setPassword("456");
		userManager.updateUser(user);
		Assert.assertEquals("456", userManager.getUserById(1).getPassword());
	}
}
