package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="DataLog")
public class DataLog implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="Activity_id")
	private Activity activity;

	@Column(name = "timestamp")
	private Date timestamp;
	
	@Column(name = "periodtime")
	private long periodtime;
	
	@Column(name="score")
	private long score;
	
	@Transient
	private String dateString;

	public String getDateString() {
		return new SimpleDateFormat("yyyy-MM-dd").format(timestamp);
	}
	
	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}




	public User getUser() {
		return user;
	}




	public void setUser(User user) {
		this.user = user;
	}




	public Activity getActivity() {
		return activity;
	}




	public void setActivity(Activity activity) {
		this.activity = activity;
	}




	public Date getTimestamp() {
		return timestamp;
	}




	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}




	public long getPeriodtime() {
		return periodtime;
	}




	public void setPeriodtime(long periodtime) {
		this.periodtime = periodtime;
	}




	public long getScore() {
		return score;
	}




	public void setScore(long score) {
		this.score = score;
	}




	
	
	
	

	@Override
	public String toString() {
		return "DataLog [activities=" + ", timestamp=" + timestamp + ", dateString=" + ", minutesSinceLastLog=" + ", positiveTime=" +  ", score=" + score + "]";
	}
	
	
}
