package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="notificationlog")
public class NotiLogData implements Serializable, Comparable<NotiLogData> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	public NotiLogData() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@Column(name="log_id")
	private long logId;

	@Column(name="user_id")
	private long id;
	
	@Column(name="noticontent")
	private String notiContent;
	
	@Column(name="timestamp", columnDefinition="DATETIME")
	private Date datetime;
	
	@Override
	public int compareTo(NotiLogData not) {
		return this.datetime.compareTo(not.datetime);
	}

	//Getters and Setters
	public long getLogId() {
		return logId;
	}

	public void setLogId(long logId) {
		this.logId = logId;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNotiContent() {
		return notiContent;
	}

	public void setNotiContent(String notiContent) {
		this.notiContent = notiContent;
	}

	public LocalDateTime getDateTime() {
		if(datetime == null) {
			return null;
		} else {
			return LocalDateTime.ofInstant(datetime.toInstant(), ZoneId.systemDefault());
		}
	}

	public void setDateTime(LocalDateTime localdatetime) {
		this.datetime = Date.from(localdatetime.atZone(ZoneId.systemDefault()).toInstant());
	}

	public Date getDate() {
		return datetime;
	}

	public void setDate(Date datetime) {
		this.datetime = datetime;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("logId: " + logId +";");
		buffer.append("id: " + id +";");
		buffer.append("notiContent: " + notiContent +";");
		buffer.append("datetime: " + datetime);
		return buffer.toString();
	}


}
