package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="Users")
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="user_id")
	private Set<DataLog> dataLogs;
	
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="user_id")
	private Set<DayLog> daylogs;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(name="user_friend",
                joinColumns={@JoinColumn(name="User_ID")},
                inverseJoinColumns={@JoinColumn(name="Friend_ID")})
    private Set<User> friends = new HashSet<User>();

	@ManyToMany(mappedBy="friends")
	private Set<User> friendsof = new HashSet<User>();
	
	public Set<User> getfriends(){
		return friends;
	}
	public Set<User> getFriends(){
		return friendsof;
	}
	public void setFriendof(Set<User> friendof){
		this.friendsof = friendof;
	}
	public void setFriends(Set<User> friend){
		this.friends = friend;
	}
	@Id
	@GeneratedValue
	@Column(name="user_id")
	private long id;
	
	public Set<DataLog> getDataLogs() {
		return dataLogs;
	}
	
	public void setDataLogs(Set<DataLog> dataLogs) {
		this.dataLogs = dataLogs;
	}

	public Set<DayLog> getDaylogs() {
		return daylogs;
	}

	public void setDaylogs(Set<DayLog> daylogs) {
		this.daylogs = daylogs;
	}
	 
	@Column(name="email", unique=true)
	private String email;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="lastName")
	private String lastName;
	
	@Column(name="userName")
	private String userName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="occupation")
	private String occupation;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="location")
	private String location;
	
	@Column(name="totalScore")
	private long totalScore;
	
	@Column(name = "startDate", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(name = "dob", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;
	

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public long getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(long totalScore) {
		this.totalScore = totalScore;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", userName=" + userName + ", password=" + password + ", occupation=" + occupation + ", gender="
				+ gender + ", location=" + location + ", totalScore=" + totalScore + ", startDate=" + startDate
				+ ", dob=" + dob + "]";
	}

	

}
