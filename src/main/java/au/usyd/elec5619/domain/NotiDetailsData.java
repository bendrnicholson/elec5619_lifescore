package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class NotiDetailsData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="user_id")
	private long id;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="neednoti")
	private boolean needNotify;
	
	@Column(name="intervaltime")
	private int intervalTime;
	
	@Column(name="notitime")
	private Date notiTime;
	
	

	//Getters and Setters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isNeedNotify() {
		return needNotify;
	}

	public void setNeedNotify(boolean neednotify) {
		this.needNotify = neednotify;
	}
	
	public int getIntervalTime() {
		return intervalTime;
	}

	public void setIntervalTime(int intervaltime) {
		this.intervalTime = intervaltime;
	}

	public LocalDateTime getNotiTime() {
		if(notiTime == null) {
			return null;
		} else {
			return LocalDateTime.ofInstant(notiTime.toInstant(), ZoneId.systemDefault());
		}
	}

	public void setNotiTime(LocalDateTime notitime) {
		this.notiTime = Date.from(notitime.atZone(ZoneId.systemDefault()).toInstant());
	}

	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("id: " + id +";");
		buffer.append("email: " + email +";");
		buffer.append("needNotify: " + needNotify +";");
		buffer.append("intervalTime: " + intervalTime +";");
		buffer.append("notiTime: " + notiTime);
		return buffer.toString();
	}

}
