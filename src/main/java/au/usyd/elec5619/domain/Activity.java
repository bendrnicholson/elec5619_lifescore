package au.usyd.elec5619.domain;
import java.io.Serializable;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="Activity")
public class Activity implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="Activity_id")
	private Set<DataLog> dataLogs;
	
	@Id
	@Column(name="Activity_id", unique=true)
	private long activityId;
	
	@Column(name="Activity_name", unique=true)
	private String activityName;
	
	@Column(name="pointsPerMinute")
	private int pointsPerMinute;
	
	

	

	public long getActivityId() {
		return activityId;
	}

	public void setActivityId(long activityId) {
		this.activityId = activityId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public Set<DataLog> getDataLogs() {
		return dataLogs;
	}

	public void setDataLogs(Set<DataLog> dataLogs) {
		this.dataLogs = dataLogs;
	}


	public int getPointsPerMinute() {
		return pointsPerMinute;
	}

	public void setPointsPerMinute(int pointsPerMinute) {
		this.pointsPerMinute = pointsPerMinute;
	}

	@Override
	public String toString() {
		return "Activity [dataLogs=" + dataLogs + ", name=" + activityName + ", pointsPerMinute=" + pointsPerMinute + "]";
	}
	
	

}
