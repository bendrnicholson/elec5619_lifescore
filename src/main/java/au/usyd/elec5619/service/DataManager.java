package au.usyd.elec5619.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.DataLog;

@Service(value="dataManager")
@Transactional
public class DataManager implements IDataManager{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addData(DataLog datalog){
		this.sessionFactory.getCurrentSession().save(datalog);
	}
	
	@Override
	public DataLog getDataById(long id) {
		
		DataLog datalog = (DataLog)this.sessionFactory.getCurrentSession().get(DataLog.class, id);

		return datalog;
	}
	
	
	

}
