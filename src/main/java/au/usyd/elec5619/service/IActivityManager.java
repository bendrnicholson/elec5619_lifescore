package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.domain.Activity;

public interface IActivityManager extends Serializable{

	public Activity getActivityById(long Activity_id);

	public List<Activity> listActivity();

	void addAct(Activity act);

	void deleteAct(long Activity_id);

	Activity findActivity(String activityName);

}
