package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Activity;
import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;

@Service(value="activityManager")
@Transactional
public class ActivityManager implements IActivityManager{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public Activity getActivityById(long Activity_id) {
		
		Activity activity = (Activity)this.sessionFactory.getCurrentSession().get(Activity.class, Activity_id);

		return activity;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Activity> listActivity() {
		// haven't tested
		return this.sessionFactory.getCurrentSession().createQuery("FROM Activity").list();
	}
	
	@Override
	public void addAct(Activity act){
		this.sessionFactory.getCurrentSession().save(act);
	}
	
	@Override
	public void deleteAct(long Activity_id){
		Session currentSession = this.sessionFactory.getCurrentSession();
		Activity activity = (Activity)currentSession.get(Activity.class, Activity_id);
		currentSession.delete(activity);
	}
	
	@Override
	public Activity findActivity(String activityName){
		  Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Activity.class);
	      criteria.add(Restrictions.like("activityName", activityName));
	      return (Activity) criteria.uniqueResult();
        
		//return (User) this.sessionFactory.getCurrentSession().get(User.class, username);
	}
	
	
	public Activity loginAct(String activityName){
		Activity activity = this.findActivity(activityName);
		if(activity !=null){
			return activity;
		}
		return null;
	}
	

}
