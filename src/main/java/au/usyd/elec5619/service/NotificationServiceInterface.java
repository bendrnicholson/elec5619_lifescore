package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.domain.NotiDetailsData;
import au.usyd.elec5619.domain.NotiLogData;



public interface NotificationServiceInterface extends Serializable {

	public NotiDetailsData getNotiDetailsDataById(long id);
	
	public void updateNotiDetailsData(NotiDetailsData notiDetailsData);
	
	public List<NotiLogData> getNotiLogDataById(long id);
	
	public void deleteNotiLogDataById(long id);
	
	public void deleteNotiLogDataByLogId(long id);
		
	public void addNotiLogData(NotiLogData notiLogData);
	
	public void switchNeedNotify (long id);
	
	public List<NotiDetailsData> getAllNotiDetailsData();
	
	public int getLogCountById(long id);

}
