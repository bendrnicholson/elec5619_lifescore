package au.usyd.elec5619.service;

import java.io.Serializable;

import au.usyd.elec5619.domain.DataLog;
import au.usyd.elec5619.domain.User;

public interface IDataManager extends Serializable{

	void addData(DataLog datalog);

	DataLog getDataById(long user_id);

	

}
