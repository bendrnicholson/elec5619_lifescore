package au.usyd.elec5619.service;

import java.io.Serializable;

import au.usyd.elec5619.domain.DayLog;

public interface IDaylogManager extends Serializable{

	DayLog getDaylogById(long user_id);

	void updateDaylog(DayLog daylog);

	void addDaylog(DayLog daylog);

}
