package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;;

@Service(value="userManager")
@Transactional
public class UserManager implements IUserManager {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionFactory  sessionFactory;

	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addUser(User user){
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	
	@Override
	public User getUserById(long id) {
		
		User user = (User)this.sessionFactory.getCurrentSession().get(User.class, id);

		return user;
	}
	

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
		// haven't tested
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();
	}
	

	
	@Override
	public User findUser(String username){
		  Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
	      criteria.add(Restrictions.like("userName", username));
	      return (User) criteria.uniqueResult();
        
		//return (User) this.sessionFactory.getCurrentSession().get(User.class, username);
	}
	
	
	public User loginUser(String username, String password){
		User user = this.findUser(username);
		if(user !=null&& user.getPassword().equals(password)){
			return user;
		}
		return null;
	}
	
	
	public void addFriend(User user, User fuser){
		user.getfriends().add(fuser);
		this.updateUser(user);	
	}
	public void removeFriend(User user, User fuser){
		user.getfriends().remove(fuser);
		user.getFriends().remove(user);
		this.updateUser(user);
	}
	
	/**
	public boolean isFriend(User user, User fuser){
	
		
	}
	*/
	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().merge(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean uniqemail(String email) {
		// TODO Auto-generated method stub
		List<User> users = (List<User>)this.sessionFactory.getCurrentSession().createQuery("From User").list();
		if(users!=null){
			for(User temp : users){
				String uemail = temp.getEmail();
				if(uemail.equals(email)){
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void removeUser(User user) {
		this.sessionFactory.getCurrentSession().delete(user);
		
	}

	@Override
	public void addDayLog(DayLog daylog) {
		// TODO Auto-generated method stub
		
	}
	
	

}
