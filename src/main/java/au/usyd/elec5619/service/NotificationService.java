package au.usyd.elec5619.service;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.NotiDetailsData;
import au.usyd.elec5619.domain.NotiLogData;

@Service(value = "notificationService")
@Transactional
public class NotificationService implements NotificationServiceInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFctory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void updateNotiDetailsData(NotiDetailsData notiDetailsData) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(notiDetailsData);
	}

	@Override
	public NotiDetailsData getNotiDetailsDataById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		NotiDetailsData notiDetailsData = (NotiDetailsData) currentSession.get(NotiDetailsData.class, id);
		return notiDetailsData;
	}
	
	@Override
	public List<NotiLogData> getNotiLogDataById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		return currentSession.createQuery("FROM NotiLogData N WHERE N.id = ?").setParameter(0, id).list();
	}

	@Override
	public void deleteNotiLogDataById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<NotiLogData> notiLogDatas = currentSession.createQuery("FROM NotiLogData N WHERE N.id = ?").setParameter(0, id).list();
		for(NotiLogData notiLogData : notiLogDatas) {
			currentSession.delete(notiLogData);
		}
	}

	@Override
	public void deleteNotiLogDataByLogId(long logId) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		NotiLogData notiLogData = (NotiLogData) currentSession.get(NotiLogData.class, logId);
		currentSession.delete(notiLogData);
	}
	
	@Override
	public void addNotiLogData(NotiLogData notiLogData) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.save(notiLogData);
	}

	@Override
	public void switchNeedNotify(long id) {
		NotiDetailsData notiDetailsData = this.getNotiDetailsDataById(id);
		if(notiDetailsData.isNeedNotify()) {
			notiDetailsData.setNeedNotify(false);
		} else {
			notiDetailsData.setNeedNotify(true);
		}
		this.updateNotiDetailsData(notiDetailsData);
	}

	@Override
	public List<NotiDetailsData> getAllNotiDetailsData() {
		Session currentSession = sessionFactory.getCurrentSession();
		return currentSession.createQuery("FROM NotiDetailsData").list();
	}

	@Override
	public int getLogCountById(long id) {
		List<NotiLogData> notiLogDatas = new ArrayList<NotiLogData>(this.getNotiLogDataById(id));
		return notiLogDatas.size();
	}

}
