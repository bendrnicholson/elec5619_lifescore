package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;

public interface IUserManager extends Serializable{


		public User getUserById(long id);
		
		public List<User> listUsers();

		
		void addUser(User user);
		
		void removeUser(User user);
		
		public void addDayLog(DayLog daylog);


		User findUser(String username);

		User loginUser(String username, String password);
		
		void updateUser(User user);
		
		public boolean uniqemail(String email);
}
