package au.usyd.elec5619.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class PostMan {
	
	public boolean sendEmail(String to, String subject, String content) {
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("elec5619lifescore","lifescore5619");
					}
				});
		
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("elec5619lifescore@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,	InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(content);

			Transport.send(message);
			
			return true;

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}



