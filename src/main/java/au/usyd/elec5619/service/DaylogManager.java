package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;

@Service(value="daylogManager")
@Transactional
public class DaylogManager implements IDaylogManager {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public DayLog getDaylogById(long user_id) {
		
		DayLog daylog = (DayLog)this.sessionFactory.getCurrentSession().get(DayLog.class, user_id);

		return daylog;
	}
	

	
	@Override
	public void updateDaylog(DayLog daylog) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.update(daylog);
	}
	
	@Override
	public void addDaylog(DayLog daylog){
		this.sessionFactory.getCurrentSession().save(daylog);
	}
	
}
