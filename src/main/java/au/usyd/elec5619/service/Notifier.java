package au.usyd.elec5619.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import au.usyd.elec5619.domain.NotiDetailsData;
import au.usyd.elec5619.domain.NotiLogData;


@Component
@EnableScheduling
public class Notifier {
	
	@Resource(name = "notificationService")
	private NotificationServiceInterface notificationServiceInterface;
	
	//Schedule Task Send Notification per 5 sec
	@Scheduled(cron = "0/5 * * * * ?")
	public void run() {
		
		System.out.println("Runing");
		
		List<NotiDetailsData> notiDetailsDatas = notificationServiceInterface.getAllNotiDetailsData();

		for(NotiDetailsData notiDetailsData : notiDetailsDatas) {
			if(notiDetailsData.isNeedNotify()) {
				if(notiDetailsData.getNotiTime() != null) {
					if(notiDetailsData.getNotiTime().plusMinutes(notiDetailsData.getIntervalTime()).compareTo(LocalDateTime.now()) < 0) {
						this.sendNotificationToLog(notiDetailsData);
						notiDetailsData.setNotiTime(LocalDateTime.now());
					}
				} else {
					notiDetailsData.setNotiTime(LocalDateTime.now());
				}
				notificationServiceInterface.updateNotiDetailsData(notiDetailsData);
			}
		}
	}

	//Schedule Task Send Email per 1 day
	@Scheduled(cron = "0 45 6,13 * * *")
	public void run2() {
		
		System.out.println("Runing2");
		
		List<NotiDetailsData> notiDetailsDatas = notificationServiceInterface.getAllNotiDetailsData();
		
		for(NotiDetailsData notiDetailsData : notiDetailsDatas) {
			if(notiDetailsData.isNeedNotify()) {
				int count = notificationServiceInterface.getLogCountById(notiDetailsData.getId());
				
				if(count > 0) {
					PostMan postman = new PostMan();
					String Email = notiDetailsData.getEmail();
					//String subject = "Hello";
					//String content = "Hello, world!";
					String subject = "Notification from LifeScore!";
					String content = "Dear " 
					+ notiDetailsData.getFirstName() + ",\r\n\r\n" 
					+ "You have " + count + " new notification(s), please login LifeScore and check.\r\n\r\n" 
					+ LocalDate.now() + " " + LocalTime.now();
					postman.sendEmail(Email, subject, content);
					this.sendEmailToLog(notiDetailsData, content);
					
				}
			}
		}
	}
	
	public void sendEmailToLog(NotiDetailsData notiDetailsData, String content) {
		NotiLogData notiLogData = new NotiLogData();
		notiLogData.setId(notiDetailsData.getId());
		notiLogData.setNotiContent("[Email Content] " + content);
		notiLogData.setDateTime(LocalDateTime.now());
		
		this.notificationServiceInterface.addNotiLogData(notiLogData);
	}
	
	
	public void sendNotificationToLog(NotiDetailsData notiDetailsData) {
		
		NotiLogData notiLogData = new NotiLogData();
		notiLogData.setId(notiDetailsData.getId());
		notiLogData.setNotiContent("Hi, it is time to upload new activity.");
		notiLogData.setDateTime(LocalDateTime.now());
		
		this.notificationServiceInterface.addNotiLogData(notiLogData);

	}

}
