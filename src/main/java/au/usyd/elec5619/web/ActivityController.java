package au.usyd.elec5619.web;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.usyd.elec5619.domain.Activity;
import au.usyd.elec5619.service.ActivityManager;

@Controller
public class ActivityController {
	@Resource(name="activityManager")
	private ActivityManager activityManager;
	
	@RequestMapping(value="editactivity", method= RequestMethod.GET)
	public String showEdit(Model model) 
	{
		List<Activity> act = activityManager.listActivity();
		model.addAttribute("act",act);
		return "editactivity";
	}
	
	@RequestMapping(value="editactivity/{id}", method= RequestMethod.GET)
	public String edit(@PathVariable("id") Long id) 
	{
		activityManager.deleteAct(id);
		return "redirect:/editactivity";
	}
	
	@RequestMapping(value="add", method= RequestMethod.GET)
	public String showadd() 
	{
		return "add";
	}
	
	@RequestMapping(value="add", method= RequestMethod.POST)
	public String add(@RequestParam("ACT") String actname,@RequestParam("VPM") int vpm, Model model) 
	{
		Activity activity = activityManager.loginAct(actname);
			if(activity != null||vpm < 0){
				 model.addAttribute("InputError","Sorry, you input invalid data,Please try agian");
				 return "add";
			 }else{ 
				 
				 Activity act = new Activity();
				act.setActivityName(actname);
				act.setPointsPerMinute(vpm);
				activityManager.addAct(act);
				return "redirect:/editactivity";}
			 
		
	}

}
