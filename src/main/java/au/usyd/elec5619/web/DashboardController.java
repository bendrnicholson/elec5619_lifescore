package au.usyd.elec5619.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.domain.DataLog;
import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;

import au.usyd.elec5619.service.UserManager;

@Controller
@SessionAttributes("uid")
public class DashboardController {
	
	
	@Resource(name="userManager")
	private UserManager userManager;


	
	
	@RequestMapping(value = "dashboard", method = RequestMethod.GET)
	public String getlistDayLogs( Model model,HttpSession session, HttpServletRequest request) {
	
		long id = (Long) session.getAttribute("uid");
		//System.out.println("dashboard" + id);
		User user = userManager.getUserById(id);
		List<DayLog> dayLogs = new ArrayList<DayLog>(user.getDaylogs());
		
		model.addAttribute("id", id);
		model.addAttribute("daylogs", dayLogs);
		model.addAttribute("userName", user.getUserName());
		System.out.println(user.getUserName());
		model.addAttribute("totalScore", user.getTotalScore());
		request.getSession().setAttribute("uid", id);
		return "dashboard";
	}


	

}
