package au.usyd.elec5619.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;


import au.usyd.elec5619.domain.Activity;
import au.usyd.elec5619.domain.DataLog;
import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.ActivityManager;
import au.usyd.elec5619.service.DataManager;
import au.usyd.elec5619.service.DaylogManager;
import au.usyd.elec5619.service.UserManager;

/**
 * Handles requests for the application add log page.
 */
@Controller
@SessionAttributes("uid")
public class AddLogController {
	
	@Resource(name="dataManager")
	private DataManager dataManager;
	
	@Resource(name="activityManager")
	private ActivityManager activityManager;
	
	@Resource(name="userManager")
	private UserManager userManager;
	
	@Resource(name="daylogManager")
	private DaylogManager daylogManager;
	

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "addlog", method = RequestMethod.GET)
	public ModelAndView addLog(HttpSession session) {

		long id = (Long) session.getAttribute("uid");
		List<Activity> activities = activityManager.listActivity();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("activities", activities);
		model.put("id",id);
		return new ModelAndView("addlog","model",model);
	}
		
	@RequestMapping(value = "addlog", method = RequestMethod.POST)
	public ModelAndView addLog(@RequestParam("activityId") int activityId, @RequestParam("periodtime") String periodtime,
			@RequestParam("date") String timestampString, @RequestParam("id") long id) throws ParseException{
		
		
		User user = userManager.getUserById(id);
		Activity activity = activityManager.getActivityById(activityId);
		DataLog datalog =  new DataLog();
		datalog.setPeriodtime(Integer.parseInt(periodtime));		
		
		//NOTE: if this doesnt work, change timestamp back to Date, setTimestamp(timestamp), and input to text (addlog.jsp)
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		 try {
			Date date = formatter.parse(timestampString);
			
			datalog.setTimestamp(date);
			//System.out.println(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 		 
		datalog.setActivity(activity);
		datalog.setUser(user);
				
		//save score to datalog
		long score = activity.getPointsPerMinute() * datalog.getPeriodtime();
		datalog.setScore(score);
		dataManager.addData(datalog);
		
		//save score to user totalscore
		long TotalScore = user.getTotalScore() + score;
		user.setTotalScore(TotalScore);
		userManager.updateUser(user);
		
		//save score to daylog dailyscore
		List<DayLog> dayLogs = new ArrayList<DayLog>(user.getDaylogs());
		boolean sign= false;

		for(int i =0; i< dayLogs.size(); i++)
		{
			String daylogString = dayLogs.get(i).getDateString();
			String datalogString = datalog.getDateString();
			if(datalogString.equals(daylogString))
			{
				//dayLogs.get(i).setUser(user);
				long dailyScore = dayLogs.get(i).getDailyScore() + score;	
				dayLogs.get(i).setDailyScore(dailyScore);
				daylogManager.updateDaylog(dayLogs.get(i));
				sign = true;
				break;
			}
		}
		
		if(sign == false){
			DayLog newdaylog = new DayLog();
			newdaylog.setUser(user);
			newdaylog.setDailyScore(score);
			Date date = formatter.parse(timestampString);
			newdaylog.setDate(date);
			daylogManager.addDaylog(newdaylog);	
		}
				
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("Name", activity.getActivityName());
		model.put("Time", datalog.getPeriodtime());
		model.put("score", datalog.getScore());
		model.put("id", user.getId());
		  
		return new ModelAndView("addlogsuccess", "model", model);
	}
	
}
