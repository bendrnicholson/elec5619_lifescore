package au.usyd.elec5619.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import au.usyd.elec5619.domain.NotiLogData;
import au.usyd.elec5619.service.NotificationServiceInterface;

@Controller
@SessionAttributes("uid")
public class NotificationLogController {
	
	@Resource(name = "notificationService")
	private NotificationServiceInterface notificationServiceInterface;
	
	@RequestMapping(value = "/notification/log/", method = RequestMethod.GET)
	public String getNotificationLogById(HttpSession httpsession, Model model) {

		long id = (Long) httpsession.getAttribute("uid");
		
		List<NotiLogData> notiLogDatas = new ArrayList<NotiLogData>(notificationServiceInterface.getNotiLogDataById(id));
		
		Collections.reverse(notiLogDatas);

		model.addAttribute("list", notiLogDatas);
		return "notificationlog";
		
	}
	
	@RequestMapping(value = "/notification/log/delete/{logId}", method = RequestMethod.POST)
	public String deleteNotificationLogByLogId(@PathVariable("logId") long logId, Model model) {
		
		notificationServiceInterface.deleteNotiLogDataByLogId(logId);
		
		return "redirect:/notification/log/";
		
	}
	
	@RequestMapping(value = "/notification/log/clearlog", method = RequestMethod.POST)
	public String deleteNotificationLogById(HttpSession httpsession, Model model) {
		
		long id = (Long) httpsession.getAttribute("uid");
		
		notificationServiceInterface.deleteNotiLogDataById(id);
		
		return "redirect:/notification/log/";
		
	}

}
