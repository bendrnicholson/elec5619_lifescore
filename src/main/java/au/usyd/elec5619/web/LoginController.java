package au.usyd.elec5619.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mysql.jdbc.Constants;

import au.usyd.elec5619.domain.User;

import au.usyd.elec5619.service.UserManager;

@Controller
@SessionAttributes("uid")
public class LoginController {

	@Resource(name="userManager")
	private UserManager userManager;

	private User user;
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginForm(){
		return "login";
	}
	@RequestMapping(value ="/admin",method = RequestMethod.GET)
	public String showAdmin(Model model){
		List<User> ulist = userManager.listUsers();
		model.addAttribute("ulist",ulist);
		return "admin";
	}
	@RequestMapping(value ="/admin",method = RequestMethod.POST)
	public String Admin(@RequestParam("name") String username, Model model){
		if(userManager.findUser(username).equals(null)||username.length()==0){
			model.addAttribute("adminmess", "User does not exist");
			List<User> ulist = userManager.listUsers();
			model.addAttribute("ulist",ulist);
			return "admin";
		}else{
		userManager.removeUser(userManager.findUser(username));
		List<User> ulist = userManager.listUsers();
		model.addAttribute("ulist",ulist);
		model.addAttribute("adminmess", "DeleteSucess");
		return "admin";
		}
	}
	@RequestMapping(value ="/login", method = RequestMethod.POST)
	public String verifyLogin(HttpServletRequest request,
            HttpServletResponse response,@RequestParam("name") String username, 
			@RequestParam("password") String password, Model model, RedirectAttributes rd, HttpSession session){
		 user = userManager.loginUser(username,password);
		if(user == null){
			 model.addAttribute("loginError","Your Username or Passwork might be wrong, try agian");
			 return "login";
		 }else if(username.equals("admin")){
			 return "redirect:/admin";
		 }
			else{
			// rd.addAttribute("name", username);
			model.addAttribute("name", username);
			long uid = user.getId();
			request.getSession().setAttribute("uid", uid);
			long tid = (Long) session.getAttribute("uid");
		
			 return "redirect:/dashboard";
		 }
	}
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String showProfile(Model model){
		model.addAttribute("name",user.getUserName());
		model.addAttribute("password",user.getPassword());
		model.addAttribute("email",user.getEmail());
		model.addAttribute("std",user.getStartDate());
		model.addAttribute("gender",user.getGender());
		model.addAttribute("ocp",user.getOccupation());
		model.addAttribute("loca",user.getLocation());
		model.addAttribute("fn",user.getFirstName());
		model.addAttribute("ln",user.getLastName());
		model.addAttribute("dob",user.getDob());
	
		
		return "profile";
	}
	@RequestMapping(value="/profile",method = RequestMethod.POST)
	public String editProfile(@RequestParam("name") String username,@RequestParam("email") String email, RedirectAttributes rd,
			@RequestParam("firstname") String fn,@RequestParam("lastname") String ln,@RequestParam("gender") String gnd,@RequestParam("occupation") String ocp,
			@RequestParam("location") String loca,@RequestParam("dateB") String dob,Model model){
		
		if(email.length()==0){
			user.setEmail(user.getEmail());
		}else if(!isEmailValid(email)){
			model.addAttribute("name",user.getUserName());
			model.addAttribute("password",user.getPassword());
			model.addAttribute("email",user.getEmail());
			model.addAttribute("std",user.getStartDate());
			model.addAttribute("gender",user.getGender());
			model.addAttribute("ocp",user.getOccupation());
			model.addAttribute("loca",user.getLocation());
			model.addAttribute("fn",user.getFirstName());
			model.addAttribute("ln",user.getLastName());
			model.addAttribute("dob",user.getDob());
			model.addAttribute("ProfileError", "The eamil address is invalid");
			return "profile";
		}
		if(!email.equals(user.getEmail())&&!userManager.uniqemail(email)){
			    model.addAttribute("name",user.getUserName());
				model.addAttribute("password",user.getPassword());
				model.addAttribute("email",user.getEmail());
				model.addAttribute("std",user.getStartDate());
				model.addAttribute("gender",user.getGender());
				model.addAttribute("ocp",user.getOccupation());
				model.addAttribute("loca",user.getLocation());
				model.addAttribute("fn",user.getFirstName());
				model.addAttribute("ln",user.getLastName());
				model.addAttribute("dob",user.getDob());
			 model.addAttribute("ProfileError", "Sorry, the email has been used");
			 return "profile";
		 }
		user.setEmail(email);
		user.setFirstName(fn.toString());
		user.setLastName(ln.toString());
		user.setGender(gnd.toString());
		user.setOccupation(ocp.toString());
		user.setLocation(loca.toString());
		//user.setDob(dob);
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
		 try {
			Date date = formatter.parse(dob);
			user.setDob(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		userManager.updateUser(user);
		rd.addFlashAttribute("ProfileError", "edit success");
		 return "suc";
		
	}
	
	@RequestMapping(value="/friend",method=RequestMethod.GET)
	public String showFriend(Model model){
		Set<User> flist =  user.getfriends();
		model.addAttribute("flist",flist);
		model.addAttribute("uname", user.getFirstName());
		return "friend";	
	}
	public boolean isFriend(User fuser){
		Set<User> flist= new HashSet<User>();
		try{
		flist = user.getfriends();
		}catch(NullPointerException ex){
			
		}
		
		for(User u:flist){
			if(u.getUserName().equals(fuser.getUserName())){
				return true;
			}
		}
		
		return false;
	}
	@RequestMapping(value="/friend",method=RequestMethod.POST)
	public String addFriend(@RequestParam("name") String username, Model model,HttpSession session){
		User temp = new User();
		temp = userManager.findUser(username);
		//long uid = (Long) session.getAttribute("uid");
		Set<User> flist= new HashSet<User>();
		System.out.println("pass1" );
		try{
		flist = user.getfriends();
		}catch(NullPointerException ex){
			
		}
		System.out.println("pass2" );
		if(flist==null){
			System.out.println("test flist is null" );
		}else{
		for(User u:flist){
			System.out.println("test flist " + u.getUserName());
		}
		}
		if(isFriend(temp)){
			model.addAttribute("fname", "You two have been friend for a while");
			return "friend";
		}else if(username.equals(user.getUserName())){
			model.addAttribute("fname", "You cant be friend with yourself");
			return "friend";
		}else if(username.length()==0){
			model.addAttribute("fname", "you have to enter a username");
			return "friend";
		}else if(temp!=null&&temp.getUserName().equals(username)){
			
			userManager.addFriend(user,temp);
			Set<User> flist2 =  user.getfriends();
			model.addAttribute("flist",flist2);
			model.addAttribute("uname", user.getFirstName());
			model.addAttribute("fname", "Congratz, you are friends now");
			return "friend";
			
		}
		else{
		model.addAttribute("ferror", "Sorry, user is not exist");
		return "friend";
		}
		
	}
	
	@RequestMapping(value="/dfriend",method= RequestMethod.GET)
	public String dfriend(Model model){
		Set<User> flist2 =  user.getfriends();
		model.addAttribute("flist",flist2);
		return "dfriend";
	}
	@RequestMapping(value="/dfriend",method= RequestMethod.POST)
	public String dfriend(Model model,@RequestParam("name") String username){
		if(username.length()==0){
			model.addAttribute("dfmess","enter a username pls");
			Set<User> flist2 =  user.getfriends();
			model.addAttribute("flist",flist2);
			return "afriend";
		}
		else if(userManager.findUser(username).equals(null)){
			model.addAttribute("dfmess","Username is invalid");
			Set<User> flist2 =  user.getfriends();
			model.addAttribute("flist",flist2);
			return "afriend";
		}else if(!this.isFriend(userManager.findUser(username))){
			model.addAttribute("dfmess","He is not your mate");
			Set<User> flist2 =  user.getfriends();
			model.addAttribute("flist",flist2);
			return "afriend";
		}else{
			User temp =userManager.findUser(username);
			userManager.removeFriend(user, temp);
			userManager.removeFriend(temp,user);
			userManager.updateUser(user);
			Set<User> flist3 =  user.getfriends();
			model.addAttribute("dfmess","It is sad that you dismissed him");
			model.addAttribute("flist",flist3);
			return "dfriend";
		}
	
	}
	
	@RequestMapping(value ="/logout", method = RequestMethod.POST)
	public String Logout(HttpSession session){
		user=null;
		session.removeAttribute("uid");
		
		return "login";
		 }
	
	public boolean isEmailValid(String email) {
		boolean isEmailValid = false;
		try {
			InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
			isEmailValid = true;
		} catch (AddressException e) {
			e.printStackTrace();
		}
		return isEmailValid;
	}

}
