package au.usyd.elec5619.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import au.usyd.elec5619.service.NotificationServiceInterface;

public class HandlerInterceptor extends HandlerInterceptorAdapter {
	
	@Resource(name = "notificationService")
	private NotificationServiceInterface notificationServiceInterface;

	//@Override
	public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) {

        if(modelAndView != null) {
        	if(modelAndView.getModelMap().containsKey("uid")) {
        		long id = (Long) modelAndView.getModelMap().get("uid");
        		int count = this.notificationServiceInterface.getLogCountById(id);
        		if(count != 0) {
        			modelAndView.getModelMap().addAttribute("logCount", count);
        		} else {
        			modelAndView.getModelMap().addAttribute("logCount", "");
        		}
        	}
        }

	}

}
