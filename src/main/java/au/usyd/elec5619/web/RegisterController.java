package au.usyd.elec5619.web;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;
import au.usyd.elec5619.service.DaylogManager;
import au.usyd.elec5619.service.NotificationServiceInterface;
import au.usyd.elec5619.service.UserManager;

@Controller
public class RegisterController {
	@Resource(name = "notificationService")
	private NotificationServiceInterface notificationServiceInterface;
	
	@Resource(name="userManager")
	private UserManager userManager;
	
	@Resource(name="daylogManager")
	private DaylogManager daylogManager;
	
	@RequestMapping(value = "/regist", method = RequestMethod.GET)
	public String showLoginForm(){
		return "register";
	}
	public boolean isEmailValid(String email) {
		boolean isEmailValid = false;
		try {
			InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
			isEmailValid = true;
		} catch (AddressException e) {
			e.printStackTrace();
		}
		return isEmailValid;
	}
	@RequestMapping(value ="/regist", method = RequestMethod.POST)
	public String verifyRegist(@RequestParam("name") String username, 
			@RequestParam("password") String password,@RequestParam("email") String email,Model model,RedirectAttributes rd){
		 User user = userManager.findUser(username);
		 if(user != null){
			 model.addAttribute("RegistError","Sorry, username has been taken");
			 return "register";
		 }else if(!isEmailValid(email)){
			 model.addAttribute("RegistError","Sorry, ur email is invalid");
			 return "register";
		 }else if(!userManager.uniqemail(email)){
			 model.addAttribute("RegistError", "Sorry, the email has been used");
			 return "register";
		 }else if(password.length()==0){
			 model.addAttribute("RegistError", "u must enter a password");
			 return "register";
		 }
		 else{
			 user= new User();
			 user.setUserName(username);
			 user.setEmail(email);
			 user.setPassword(password);
			 java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
			 user.setStartDate(date);
			 userManager.addUser(user);
			 rd.addFlashAttribute("loginerror", "Regist Success");
			 DayLog daylog = new DayLog();
			 daylog.getUser();
			 daylog.setUser(user);
			 daylog.setDate(date);
			 daylog.setDailyScore(0);
			 daylogManager.addDaylog(daylog);
			 return "redirect:/login";
		 }
		 
		
	}
	

}