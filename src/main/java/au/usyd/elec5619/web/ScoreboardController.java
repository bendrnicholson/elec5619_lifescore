package au.usyd.elec5619.web;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.domain.DataLog;
import au.usyd.elec5619.domain.DayLog;
import au.usyd.elec5619.domain.User;

import au.usyd.elec5619.service.UserManager;

@Controller
@SessionAttributes("uid")
public class ScoreboardController {
	
	
	@Resource(name="userManager")
	private UserManager userManager;

	
	
	@RequestMapping(value = "scoreboard", method = RequestMethod.GET)
	public String getUserScores( Model model,HttpSession session) {
		long id = (Long) session.getAttribute("uid");
		User user = userManager.getUserById(id);
		List<User> allUsers = userManager.listUsers();
		Set<User> friends = user.getfriends();
		
		//System.out.println("ABOUT TO SORT, friends size = " + friends.size());
		//allUsers = filterByLocation(allUsers, user.getLocation());
		//allUsers = filterForFriends(allUsers, user, friends);
		allUsers = sortByTotalScore(allUsers);
		int rank = getRank(user.getUserName(),allUsers);
		model.addAttribute("rank",rank);
		model.addAttribute("id", id);
		model.addAttribute("users", allUsers);
		model.addAttribute("userName", user.getUserName());
		model.addAttribute("occupation", user.getOccupation());
		model.addAttribute("totalScore", user.getTotalScore());
		//System.out.println("SUCCESS IN JAVA LENGTH OF ARRAY IS" + allUsers.size());
		return "scoreboard";
	}
	@RequestMapping(value = "scoreboard", method = RequestMethod.POST)
	public String filter( Model model,HttpSession session, @RequestParam("filter") String filter, @RequestParam("id") long id){
		System.out.println("RECEIVED");
		System.out.println("ID"+ id);
		//Map<String, Object> model = new HashMap<String, Object>();
		User user = userManager.getUserById(id);
		List<User> allUsers = userManager.listUsers();
		//System.out.println("number of users" + allUsers.size());
		Set<User> friends = user.getfriends();
		if(filter.compareTo("friends") == 0){
			allUsers = filterForFriends(allUsers, user, friends);
			model.addAttribute("filterName", "Comparing your score against your friends");
		}
		if(filter.compareTo("location") == 0){
			allUsers = filterByLocation(allUsers, user.getLocation());
			model.addAttribute("filterName", "Comparing your score against your city");
		}
		if(filter.compareTo("occupation") == 0){
			allUsers = filterByOccupation(allUsers, user.getOccupation());
			model.addAttribute("filterName", "Comparing your score against people with the same job");
		}
		if(filter.compareTo("global") == 0){
			model.addAttribute("filterName", "Comparing your score against everyone");
		}
		
		
		//System.out.println("number of friends" + allUsers.size());
		allUsers = sortByTotalScore(allUsers);
		int rank = getRank(user.getUserName(),allUsers);
		model.addAttribute("id",id);
		model.addAttribute("rank",rank);
		model.addAttribute("users", allUsers);
		return "scoreboard";
	}
	
	
	public List<User> sortByTotalScore(List<User> users){
		boolean sorted = false;
		//System.out.println("SORTING");
		int length = users.size()-1;
		//System.out.println("length =" + length );
		if(length == 0){
			return users;
		}
		if(length == 1){
			if(users.get(0).getTotalScore() < users.get(1).getTotalScore()){
			//	System.out.println("LENGTH 1 SWAPPING");
				User temp = users.get(0);
				users.set(0, users.get(1));
				users.set(1, temp);
			}
			sorted = true;
			//System.out.println("SUCCESS sorting length 1");
			return users;
		}
		while(sorted == false){
			//System.out.println("ONE PASS");
			sorted = true;
			for(int i = 0; i < length; i++){
			//	System.out.println("COMPARING" + users.get(i).getTotalScore() +"against" + users.get(i+1).getTotalScore());
				if(users.get(i).getTotalScore() < users.get(i+1).getTotalScore()){
			//		System.out.println("SWAPPING");
					sorted = false;
					User temp = users.get(i);
					users.set(i, users.get(i+1));
					users.set(i+1, temp);
				}
			}
		}
		
		return users;
	}

	public List<User> filterByLocation(List<User> users, String location){
		List<User> filteredUsers = new ArrayList<User>();
		int length = users.size()-1;
		//System.out.println("length =" + length );
			for(int i = 0; i <= length; i++){
				if(users.get(i).getLocation().compareTo(location) != 0){
		//			System.out.println("Filtering");
				}
				else{
					filteredUsers.add(users.get(i));
		//			System.out.println("LETTING IN");
				}
		}
		
		return filteredUsers;
	}

	public List<User> filterByOccupation(List<User> users, String occupation){
		List<User> filteredUsers = new ArrayList<User>();
		int length = users.size()-1;
		//System.out.println("length =" + length );
			for(int i = 0; i <= length; i++){
				if(users.get(i).getOccupation().compareTo(occupation) != 0){
		//			System.out.println("Filtering");
				}
				else{
					filteredUsers.add(users.get(i));
		//			System.out.println("LETTING IN");
				}
		}
		
		return filteredUsers;
	}
	public List<User> filterForFriends(List<User> users, User u, Set<User> friends){
		List<User> filteredUsers = new ArrayList<User>();
		int length = users.size()-1;
		String[] arrayFriends = new String[friends.size()];
		int index = 0;
		for(User f: friends){
		//	System.out.println(f.getUserName());
			arrayFriends[index] = f.getUserName();
			index ++;
		}
		//System.out.println("length =" + length );
			for(int i = 0; i <= length; i++){
		//		System.out.println("CHECKING FRIENDLYNESS");
				for(int j = 0; j < friends.size(); j ++){
					if(arrayFriends[j].compareTo(users.get(i).getUserName()) == 0){
		//				System.out.println("IsFriend");
						filteredUsers.add(users.get(i));
					}
					else{
		//				System.out.println("Filtering");
					}
				}
				
		}
	    filteredUsers.add(u);
		return filteredUsers;
	}
	
	public int getRank(String username, List<User> users){
		int length = users.size()-1;
		int rank = 0;
		for(int i = 0; i <= length; i++){
			if(users.get(i).getUserName().compareTo(username) == 0){
				rank = i+1;
			}
		}
		return rank;
	}
	

}
