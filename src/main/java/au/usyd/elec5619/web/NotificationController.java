package au.usyd.elec5619.web;


import java.time.LocalDate;
import java.time.LocalTime;

import javax.annotation.Resource;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import au.usyd.elec5619.domain.NotiDetailsData;
import au.usyd.elec5619.service.NotificationServiceInterface;
import au.usyd.elec5619.service.PostMan;

@Controller
@SessionAttributes("uid")
public class NotificationController {
	
	private int indicatorEmail = 0;
	private int indicatorIntervalTime = 0;
	
	@Resource(name = "notificationService")
	private NotificationServiceInterface notificationServiceInterface;
	
	@RequestMapping(value = "/notification/", method = RequestMethod.GET)
	public String getNotiDetial(HttpSession httpsession, Model model) {
		
		long id = (Long) httpsession.getAttribute("uid");
	
		NotiDetailsData notiDetailsData = new NotiDetailsData();
		notiDetailsData = this.notificationServiceInterface.getNotiDetailsDataById(id);
		
		model.addAttribute("id", notiDetailsData.getId());
		model.addAttribute("currentEmail", notiDetailsData.getEmail());
		model.addAttribute("needNotify", notiDetailsData.isNeedNotify());
		model.addAttribute("intervalTime", notiDetailsData.getIntervalTime());
		model.addAttribute("indicatorEmail", this.indicatorEmail);
		model.addAttribute("indicatorIntervalTime", this.indicatorIntervalTime);
		this.indicatorEmail = 0;
		this.indicatorIntervalTime = 0;

		return "notification";
	}
	
	
	@RequestMapping(value = "/notification/email", method = RequestMethod.POST)
	public String postEmail(@RequestParam("newEmail") String newEmail, HttpSession httpsession, Model model) {
		long id = (Long) httpsession.getAttribute("uid");
		
		NotiDetailsData notiDetailsData = this.notificationServiceInterface.getNotiDetailsDataById(id);
		
		if(this.isEmailValid(newEmail)) {
			notiDetailsData.setEmail(newEmail);
			this.notificationServiceInterface.updateNotiDetailsData(notiDetailsData);
			this.indicatorEmail = 1;
			PostMan postman = new PostMan();
			//String subject = "Hello";
			//String content = "Hello, world!";
			String subject = "Greeting from LifeScore!";
			String content = "Dear " 
			+ this.notificationServiceInterface.getNotiDetailsDataById(id).getFirstName() + ",\r\n\r\n" 
			+ "Your Email address has been successfully updated." + "\r\n\r\n" 
			+ LocalDate.now() + " " + LocalTime.now();
			postman.sendEmail(newEmail, subject, content);
			
		} else {
			this.indicatorEmail = 2;
		}
		return "redirect:/notification/";
	}
	
	@RequestMapping(value = "/notification/interval", method = RequestMethod.POST)
	public String postIntervalTime(@RequestParam("newIntervalTime") String newIntervalTimeS, HttpSession httpsession, Model model) {
		long id = (Long) httpsession.getAttribute("uid");
		
		NotiDetailsData notiDetailsData = this.notificationServiceInterface.getNotiDetailsDataById(id);
		
		if(this.isIntervalTimeValid(newIntervalTimeS)) {
			int newIntervalTime = Integer.parseInt(newIntervalTimeS);
			notiDetailsData.setIntervalTime(newIntervalTime);
			this.notificationServiceInterface.updateNotiDetailsData(notiDetailsData);
			this.indicatorIntervalTime = 1;
		} else {
			this.indicatorIntervalTime = 2;
		}
		return "redirect:/notification/";
	}
	
	@RequestMapping(value = "/notification/switch", method = RequestMethod.POST)
	public String switchNeedNotify(HttpSession httpsession, Model model) {
		long id = (Long) httpsession.getAttribute("uid");
		this.notificationServiceInterface.switchNeedNotify(id);
		return "redirect:/notification/";
	}
	
	public boolean isEmailValid(String email) {
		boolean isEmailValid = false;
		try {
			InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
			isEmailValid = true;
		} catch (AddressException e) {
			e.printStackTrace();
		}
		return isEmailValid;
	}
	
	public boolean isIntervalTimeValid(String newIntervalTimeS) {
		if(newIntervalTimeS.trim() != "") {
			char[] chars = newIntervalTimeS.toCharArray();
			for(char ch : chars) {
				if(!Character.isDigit(ch)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
}
