<%@ include file="/WEB-INF/views/include.jsp" %>
   <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

   <link rel="stylesheet" href="resources/addlog/css/style.css">
<body>
	
	<div class="container">
	
	<h1>Regist</h1> 
	<p style="font-family:arial;color:red;font-size:20px;">${RegistError}</p>
	<form class="row" action="regist" method="post">
		<div class="form-group  col-xs-12">
		    <label>UserName:</label>
		    <input type="text" class="form-control" name="name" size="30" maxlength="25" placeholder="Username">
		</div>
		<div class="form-group col-xs-12">
		    <label>Password:</label>
		    <input type="password" class="form-control" name="password" size="30" maxlength="25" placeholder="Password">
		</div>
		<div class="form-group col-xs-12">
		    <label>Email:</label>
		    <input type="text" class="form-control" name="email" size="30" maxlength="40" placeholder="Email">
		</div>
		
		<div class="form-group col-xs-12">
			<input type="submit" class="btn btn-success"/>
		</div>
		
	</form>

	
	</div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
	 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="resources/addlog/js/index.js"></script>
</body>