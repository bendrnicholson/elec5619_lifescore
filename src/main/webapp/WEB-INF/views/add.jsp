<%@ include file="/WEB-INF/views/include.jsp" %>
   <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
   <link rel="stylesheet" href="resources/addlog/css/style.css">
<body>
	<nav class="navbar navbar-static-top navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Lifescore</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
            
              <li><a href="/elec5619/admin" >Edit User</a></li>
              <li><a href="/elec5619/editactivity">Edit Activity</a></li>
              <li><a href="/elec5619/add">Add Activity</a></li>
              <li><a href="<c:url value="login.htm"/>">LogOut</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
	
	<div class="container">
	<br>
	
	<h1>Add Activity</h1> 
	<br>
	<br>
	<form class="row" action="add" method="post">
		<div class="form-group  col-xs-6">
		    <label>Activity Name:</label>
		    <input type="text" class="form-control" name="ACT" required >
		</div>
		<div class="form-group col-xs-6">
		    <label>Value Per Min:</label>
		    <input type="number" class="form-control" name="VPM" required>
		</div>
		<span class="alert">Please fill all fields</span>
		<div class="form-group col-xs-6">
			<input type="submit" class="btn btn-success"/>
			<p style="color:red;">${InputError}</p>
		</div>
		
	</form>
	</div>
	
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="resources/addlog/js/index.js"></script>

