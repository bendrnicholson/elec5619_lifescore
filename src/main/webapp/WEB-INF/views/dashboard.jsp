<%@ include file="/WEB-INF/views/include.jsp"%>

<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="resources/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="resources/fonts.css" rel="stylesheet" type="text/css" media="all" />

<link rel="stylesheet" href="resources/assets/css/main.css" />
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Score'],
          <c:forEach items="${daylogs}" var="daylog">
          ['${daylog.dateString}', ${daylog.dailyScore}],
          </c:forEach>
          
        ]);

        var options = {
          title: 'Daily Performance',
          curveType: 'function',
          legend: { position: 'bottom' },
          backgroundColor: {
              'fill': '#FCF3CF',
              
           },

        };
        
        
        
        data.sort({ column: 0, asc: true });
        
        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
 
 <style>

body 
{
	background-image:url('resources/freebg1.jpg');
	background-color:#cccccc;
}
</style>
 
 
 
<body>

<%@ include file="/WEB-INF/views/header.jsp"%>
<%--
	<nav class="navbar navbar-static-top navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Lifescore</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
            
              <li><a href="/elec5619/dashboard?id=${id}" >Dashboard</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Scoreboard</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="<c:url value="friend"/>">Friends</a></li>
              <li><a href="/elec5619/addlog?id=${id}">Add Log</a></li>
              <li><a href="<c:url value="login"/>">LogOut</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
--%>

<%-- 	 <div class="row">
                <div class="col-lg-2 col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge"><b>Welcome:  ${userName}</b> </div>
                                    <div><b>Your TotalScore: ${totalScore}</b></div>
                                </div>
                            </div>
                        </div>
                      </div>
                  </div>
     </div> --%>
 
	<input type="hidden" value="${id}" name="id">
	
	<!-- <div id="curve_chart" style="width: 900px; height: 500px"></div> -->
	
	
	<div id="portfolio" class="container">
		<div class="title">
		
			<div class="column2">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
				<div class="box"> <span class="icon icon-user"></span>
					<h3><b>Welcome:  ${userName}</b></h3>
					<p><b>Your TotalScore: ${totalScore}</b></p>
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<div class="col-lg-5 col-md-3"><div id="curve_chart" style="width: 900px; height: 500px"></div></div>
		</div>
	</div>

	
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>

</body>
