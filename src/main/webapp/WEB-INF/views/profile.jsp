<%@ include file="/WEB-INF/views/include.jsp" %>
 
  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

   <link rel="stylesheet" href="resources/addlog/css/style.css">
<body>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<div class="container">
	
	<h1>Profile</h1> 
	<p style="font-family:arial;color:red;font-size:20px;">${ProfileError}</p>
	<form class="row" action="profile" method="post">
		<div class="form-group  col-xs-6">
		    <label>UserName:</label>
		    <input type="text" class="form-control" name="name" size="30" maxlength="25" placeholder="${name}" readonly>
		</div>
		<div class="form-group col-xs-6">
		    <label>Password:</label>
		    <input type="password" class="form-control" name="password" size="30" maxlength="25" placeholder="${password}">
		</div>
		<div class="form-group col-xs-6">
		    <label>Email:</label>
		    <input type="text" class="form-control" name="email" size="30" maxlength="25" value="${email}" >
		</div>
		<div class="form-group col-xs-6">
		    <label>FirstName:</label>
		    <input type="text" class="form-control" name="firstname" size="30" maxlength="25" value="${fn}" placeholder="${fn}">
		</div>
		<div class="form-group col-xs-6">
		    <label>LastName:</label>
		    <input type="text" class="form-control" name="lastname" size="30" maxlength="25" value="${ln}">
		</div>
		<div class="form-group col-xs-6">
		    <label>StartDate</label>
		    <input type="text" class="form-control" name="std" size="30" maxlength="25" placeholder="${std}" readonly>
		</div>
		<div class="form-group col-xs-6">
		    <label>Gender</label>
		    <input list = "genderlist" type="text" class="form-control" name="gender" size="30" maxlength="25" value="${gender}">
			<datalist id="genderlist">
				<option value="Female">
				<option value="Male">
			</datalist>
		</div>
		<div class="form-group col-xs-6">
		    <label>Occupation</label>
		    <input type="text" class="form-control" name="occupation" size="30" maxlength="25" value="${ocp}">
		</div>
		<div class="form-group col-xs-6">
		    <label>Location:</label>
		    <input type="text" class="form-control" name="location" size="30" maxlength="25" value="${loca}">
		</div>
		<div class="form-group col-xs-6">
			
			<label class="control-label" for="date">Birthday</label>
        	<input class="form-control" id="date" name="dateB" placeholder="${dob}" type="text"/>
		</div>
		<div class="form-group col-xs-6">
			<button style="float: right;" name = "Confirm" type="submit" class="btn btn-success"/>Edit</Button>
		</div>
		
	</form>

	
	</div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="resources/addlog/js/index.js"></script>
<!-- Include jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        var date_input=$('input[name="dateB"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>   