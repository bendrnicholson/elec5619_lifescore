<nav class="navbar navbar-static-top navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Lifescore</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
            
              <li><a href="/elec5619/dashboard?id=${id}" >Dashboard</a></li>
              <li><a href="/elec5619/notification/log/">Messages <span class="badge">${logCount}</span></a></li>
              <li><a href="#">Scoreboard</a></li>
              <li><a href="<c:url value="profile"/>">Profile</a></li>
              <li><a href="<c:url value="friend"/>">Friend</a></li>
              <li><a href="/elec5619/addlog?id=${model.id}">Add Log</a></li>
              <li><a href="/elec5619/notification/">Notification Setting</a></li>
              <li><a href="<c:url value="login.htm"/>">LogOut</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>