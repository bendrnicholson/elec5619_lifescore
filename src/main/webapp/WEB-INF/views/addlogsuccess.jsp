<%@ include file="/WEB-INF/views/include.jsp"%>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />




<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="resources/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="resources/fonts.css" rel="stylesheet" type="text/css" media="all" />

<style>

body 
{
	background-image:url('resources/freebg1.jpg');
	background-color:#cccccc;
}
</style> 

  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

   <link rel="stylesheet" href="resources/addlog/css/style.css">

<body>
	<nav class="navbar navbar-static-top navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Lifescore</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
            
              <li><a href="/elec5619/dashboard?id=${model.id}" >Dashboard</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Scoreboard</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Settings</a></li>
              <li><a href="/elec5619/addlog?id=${model.id}">Add Log</a></li>
              <li><a href="<c:url value="login.htm"/>">LogOut</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
	
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>
	

	<div id="welcome" class="container">
	<div class="title">
			<h2>Record Created</h2>
			<span class="byline">Good Job!</span> </div>
		
		<div class="title">
			<p class="h2">You select <b> ${model.Name}</b></p>	
			<p class="h2">And you spend <b>${model.Time}</b> minutes on it</p>
	        <p class="h2">We estimates you will get: <b>${model.score}</b> points</p>
	        <br>
			<a href="/elec5619/dashboard?id=${model.id}" class="btn btn-success">Back To Home</a> </div>
	</div>
	
	
	
	
				
	
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>
	 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="resources/addlog/js/index.js"></script>
	

</body>
