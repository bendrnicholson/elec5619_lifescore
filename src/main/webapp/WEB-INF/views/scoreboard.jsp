<%@ include file="/WEB-INF/views/include.jsp"%>

<body>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<div class="container">
			<h1>Scoreboard </h1>
			<h1>Congratulations, you are rank #${rank}</h1>
			<form class="row" action="scoreboard" method="post" name="Form">
			<div class="login-form">
			<div class="form-group log-status">
					<select name="filter" class="form-control" required>
						<option value ="">Choose value to filter scoreboard</option>
							<option value="friends">Compare with your friends</option>
							<option value="location">Compare with people in your city</option>
							<option value="occupation">Compare with people who have the same job as you</option>
							<option value="global">Compare with everyone</option>
						
					</select>
			</div>
			<button type="submit" class="log-btn">Submit</button>
				
				<input type="hidden" value="${id}" name="id">
			</div>
		</form>
			
			
			<table class="table table-striped">
 				<caption>${filterName}</caption>
 				<thead>
 					<tr>
 						<th>#</th>
 						<th>Name</th>
 						<th>Score</th>
 						<th>Occupation</th>
 						<th>Location</th>
 						</tr>
 						</thead>
 						<tbody>
 						<% System.out.println( "ABOUT TO MAKE LIST"); %>
 						<c:set var="count" value="1" scope="page" />
 						<c:forEach items="${users}" var="u">
 							<tr>
 								<th scope="row">${count}</th>
 								<td>${u.getUserName()}</td>
 								<td>${u.getTotalScore()}</td>
 								<td>${u.getOccupation()}</td>
 								<td>${u.getLocation()}</td>
 							</tr>
 							<c:set var="count" value="${count + 1}" scope="page"/>
 							</c:forEach>
 						</tbody>
 				
			</table>
	</div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>
</body>