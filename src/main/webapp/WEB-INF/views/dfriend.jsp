<%@ include file="/WEB-INF/views/include.jsp" %>
   <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

   <link rel="stylesheet" href="resources/addlog/css/style.css">
<body>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<div class="container">
	<h1>Manage your friend here</h1>
	<h1>${dfmess}</h1>
	<form class="row" action="dfriend" method="post">
		
		<div class="form-group  col-xs-12">
		   
		    <br>
		    <br>
		    <input type="text" class="form-control" name="name" size="30" maxlength="25" placeholder=" Type Username you want to drop">
		</div>
		 
		
		<div class="form-group col-xs-12">
			<input style="float: right;"  name = "Delete" type="submit" class="btn btn-danger"/>
		</div>
		
	</form>
	</div>
	
	<h2>Friend List</h2>
	<table class="table table-bordered">
	<thead>	
		<tr>
			<th class="success">FirstName</th>
			<th class="warning">LastName</th>
			<th class="info">UserName</th>
		</tr>
	</thead>
	<tbody>
	
        <c:forEach items="${flist}" var="item" varStatus="count"> 
           		  <tr>
           		  <td class="success">${item.firstName}</td>
           		  <td class="warning">${item.lastName}</td>
           		  <td class="info">${item.userName}</td>
           		  </tr>
       </c:forEach>
  
   </tbody>
   </table>
    
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="resources/addlog/js/index.js"></script>