<%@ include file="/WEB-INF/views/include.jsp"%>


<link rel='stylesheet prefetch'
	href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

<link rel="stylesheet" href="resources/addlog/css/style.css">


<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<body>

	<nav class="navbar navbar-static-top navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Lifescore</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">

					<li><a href="/elec5619/dashboard?id=${model.id}">Dashboard</a></li>
					<li><a href="#">Messages</a></li>
					<li><a href="/elec5619/scoreboard">Scoreboard</a></li>
					<li><a href="#">Profile</a></li>
					<li><a href="#">Settings</a></li>
					<li><a href="/elec5619/addlog?id=${model.id}">Add Log</a></li>
					<li><a href="<c:url value="login.htm"/>">LogOut</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>




	<div class="container">
		<h1>Submit Activity Data</h1>

		<form class="row" action="addlog" method="post" name="Form">
	
				<!--<span class="alert">Invalid Credentials</span> -->
				<div class="login-form">
				<div class="form-group log-status">
					<select name="activityId" class="form-control" required>
						<option value ="">Select Activity</option>
						<c:forEach items="${model.activities}" var="activity">
							<option value="${activity.activityId}">${activity.activityName}</option>
						</c:forEach>
					</select>
					<form:errors path="activites" cssClass="error" />
				</div>

				<div class="form-group ">
					<input type="text" class="form-control" placeholder="Periodtime/mins"
						name="periodtime" id="time" required>
				</div>
				
				
				<div class="form-group ">
					<input type="text" class="form-control"
						placeholder="Date: e.g. MM/DD/YYYY " name="date" id="date" required>

				</div>

				<button type="submit" class="log-btn">Submit</button>
				
				<span class="alert">Please fill all fields</span>

				<input type="hidden" value="${model.id}" name="id">
			</div>
		</form>
	</div>

	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="resources/addlog/js/index.js"></script>
</body>

<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<script>
	$(document).ready(
			function() {
				var date_input = $('input[name="date"]'); //our date input has the name "date"
				var container = $('.bootstrap-iso form').length > 0 ? $(
						'.bootstrap-iso form').parent() : "body";
				date_input.datepicker({
					format : 'yyyy-mm-dd',
					container : container,
					todayHighlight : true,
					autoclose : true,
				})
			})
</script>


<!-- <script>
	var checkrForm = function() {
		var rdate = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/, rtime = /^[-+]?[1-9]\d*$/, date = document
				.getElementById('date').value, time = document
				.getElementById('time').value,

		status_hint = document.getElementById('test-error');
		if (rdate.test(date) && rtime.test(time)) {
			status_hint.innerHTML = 'success!';
			return true;
		} else {
			status_hint.innerHTML = 'try again!';
			return false;
		}
	};
</script> -->






