<%@ include file="/WEB-INF/views/include.jsp"%>

<body>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<div class="container">

		<div class="row">
			<div class="col-md-3"></div>
  			<div class="col-md-6">
  				<div class="panel panel-default">
            		<div class="panel-heading"><h3 class="panel-title"><strong>Notification</strong></h3></div>
            		<div class="panel-body">
              			<div class="row">
              				<div class="col-md-8"></div>
              				<div class="col-md-4">
              					<form method="post" action="switch">
              						<c:if test="${needNotify}"><button class="btn btn-success btn-block" type="submit">On</button></c:if>
              						<c:if test="${!needNotify}"><button class="btn btn-danger btn-block" type="submit">Off</button></c:if>
              					</form>
              				</div>
              			</div>
            		</div>
          		</div>
          		<div class="panel panel-default">
            		<div class="panel-heading"><h3 class="panel-title"><strong>Email</strong></h3></div>
            		<div class="panel-body">
              			<form class="form-horizontal" method="post" action="email">
              				<div class="form-group">
              					<label class="col-sm-4 control-label">Current Email address:</label>
              					<div class="col-md-8"><h5><c:out value="${currentEmail}"></c:out></h5></div>
              				</div>
              				<div class="form-group">
              					<label class="col-sm-4 control-label">New Email address:</label>
              					<div class="col-sm-8">
              						<input class="form-control" type="text" name="newEmail" placeholder="${currentEmail}">
              					</div>
              				</div>
              				<div class="row">
              					<div class="col-md-8">
              						<c:if test="${indicatorEmail == 1}"><p class="text-success"><strong>>> Well done! </strong>A greeting Email has been sent.</p></c:if>
              						<c:if test="${indicatorEmail == 2}"><p class="text-danger"><strong>>> Oh snap! </strong>You must enter a valid Email address.</p></c:if>
              					</div>
              					<div class="col-md-4">
              						<button class="btn btn-primary btn-block" type="submit">Submit</button>
              					</div>
              				</div>
              			</form>
            		</div>
          		</div>
          		<div class="panel panel-default">
            		<div class="panel-heading"><h3 class="panel-title"><strong>Interval Time</strong></h3></div>
            		<div class="panel-body">
            			<form class="form-horizontal" method="post" action="interval">
            				<div class="form-group">
		              			<label class="col-sm-4 control-label">Current interval time:</label>
		              			<div class="col-md-1"><h5><c:out value="${intervalTime}"></c:out></h5></div>
		              			<label class="col-sm-3 control-label">New interval:</label>
		              			<div class="col-md-4">
		              				<div class="input-group">
										<input type="text" class="form-control" name="newIntervalTime" placeholder="${intervalTime}">
										<div class="input-group-addon">min</div>
		    						</div>
		    					</div>		
	              			</div>
              				<div class="row">
              					<div class="col-md-8">
              						<c:if test="${indicatorIntervalTime == 1}"><p class="text-success"><strong>>> Well done! </strong>Interval time has successfully updated.</p></c:if>
              						<c:if test="${indicatorIntervalTime == 2}"><p class="text-danger"><strong>>> Oh snap! </strong>You must enter a valid interval time.</p></c:if>
              					</div>
              					<div class="col-md-4">
              					<button class="btn btn-primary btn-block" type="submit">Submit</button>
              					</div>
              				</div>
              			</form>
            		</div>
          		</div>
  			</div>
  			<div class="col-md-3"></div>
		</div>

	</div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>
</body>