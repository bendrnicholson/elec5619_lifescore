<%@ include file="/WEB-INF/views/include.jsp"%>

<body>
	<%@ include file="/WEB-INF/views/header.jsp"%>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
  			<div class="col-md-6">
  				<div class="bs-doc-section">
  					<div class="row">
              			<div class="col-md-8"></div>
              			<div class="col-md-4">
              				<form method="post" action="clearlog">
              					<button class="btn btn-danger btn-block" type="submit">Clear Log</button>
              				</form>
              			</div>
              		</div>
  					<c:forEach items="${list}" var="noti">
  						<form method="post" action="delete/${noti.logId}">
  							<div class="bs-callout bs-callout-info" id="callout-helper-context-color-specificity">
  								<h4><fmt:formatDate value="${noti.date}" pattern="yyyy-MM-dd HH:mm:ss" /></h4>
  								<button type="submit" class="close"><span aria-hidden="true">&times;</span></button>
  								<p><c:out value="${noti.notiContent}"/></p>
  							</div>
  						</form>
  					</c:forEach>
  				</div>
  			</div>
  			<div class="col-md-3"></div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>
</body>

