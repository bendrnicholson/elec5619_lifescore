DROP TABLE IF EXISTS `lifescore`.`NotificationLog` ;

CREATE TABLE IF NOT EXISTS `lifescore`.`NotificationLog` (
  `log_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `noticontent` TEXT(500) NULL DEFAULT NULL,
  `timestamp` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`))
ENGINE = InnoDB;

INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (1, "1-1Hi, it is time to upload new activity.", "2016-10-21");
INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (3, "3-1Hi, it is time to upload new activity.", "2016-10-22");
INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (5, "5-1Hi, it is time to upload new activity.", "2016-10-23");
INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (3, "3-2Hi, it is time to upload new activity.", "2016-10-24");
INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (7, "7-1Hi, it is time to upload new activity.", "2016-10-25");
INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (3, "3-3Hi, it is time to upload new activity.", "2016-10-26");
INSERT INTO notificationlog (user_id, noticontent, timestamp) VALUES (1, "1-2Hi, it is time to upload new activity.", "2016-10-27");
