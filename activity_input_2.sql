INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (1, "Jogging", 5);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (2, "YouTube", -1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (3, "Study", 2);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (4, "Cooking", 1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (5, "Napping", -1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (6, "Gaming", -1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (7, "Nothing", -5);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (8, "Chilling", -1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (9, "Sleeping", 0);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (10, "Reading", 2);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (11, "Movie", -1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (12, "Eating", 0);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (13, "Biking", 2);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (14, "Swimming", 1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (15, "Music", 0);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (16, "Working Out", 3);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (17, "Watching TV", -3);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (18, "Photography", 1);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (19, "Partying", -3);

INSERT INTO Activity (Activity_id, Activity_name, pointsPerMinute)
VALUES (20, "Chatting", 0);




